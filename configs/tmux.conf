set-option -g default-terminal "screen-256color"
#set-option -g default-shell /usr/bin/zsh

#Prefix is Ctrl-a
unbind C-b
set -g prefix C-a
bind C-a send-prefix

set -sg escape-time 1
set -g base-index 1
setw -g pane-base-index 1
 
#Mouse works as expected
#set -g mouse on

setw -g monitor-activity on
set -g visual-activity on
 
set -g mode-keys vi
set -g history-limit 10000
 
set-window-option -g mode-keys vi

# y and p as in vim
#bind Escape copy-mode
#unbind p
bind p paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-selection
bind-key -T copy-mode-vi r send-keys -X rectangle-toggle
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'
#bind -t vi-copy 'v' begin-selection
#bind -t vi-copy 'y' copy-selection
#bind -t vi-copy y copy-pipe "xclip -sel clip -i"
#bind -t vi-copy 'Space' halfpage-down
#bind -t vi-copy 'Bspace' halfpage-up
 
# extra commands for interacting with the ICCCM clipboard
#bind C-c run "tmux save-buffer - | xclip -i -sel clipboard"
#bind C-v run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"
 
# easy-to-remember split pane commands
bind | split-window -h -c '#{pane_current_path}'
bind - split-window -v -c '#{pane_current_path}'
unbind '"'
unbind '%'
bind '%' split-window -h
bind '"' split-window -v 
bind c new-window -c '#{pane_current_path}' # Create new window

# moving between panes with vim movement keys
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# moving between windows with vim movement keys
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# resize panes with vim movement keys
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

#urxvt tab like window switching (-n: no prior escape seq)
bind -n S-down new-window
bind -n S-left prev
bind -n S-right next
bind -n C-left swap-window -t -1
bind -n C-right swap-window -t +1
set -g set-titles on
set -g set-titles-string "#T"

#CLIPBOARD selection integration
##Requires prefix key before the command key
#Copy tmux paste buffer to CLIPBOARD
bind C-c run "tmux show-buffer | xsel -i -b"
#Copy CLIPBOARD to tmux paste buffer and paste tmux paste buffer
bind C-v run "tmux set-buffer -- \"$(xsel -o -b)\"; tmux paste-buffer"
#source "/usr/share/tmux/powerline.conf"

set -g status-bg black
set -g status-fg white
set -g status-interval 10
set -g status-left-length 30
#set -g status-left '[#I:#P]'    # Show window:pane numbers
set -g status-left '#[fg=colour237]#I:#P #[fg=white]#(whoami)@#H'
#set -g status-justify left
set -g status-right-length 90
#set -g status-right "#[fg=green]#(/usr/bin/acpi -b) #[fg=cyan]#(cut -d \" \" -f 1 /proc/loadavg) #[default]%a %d %b %R"
set -g status-right "#[fg=green]#(/usr/bin/acpi -b|awk '{print $1,$4}') #[fg=cyan]#(df -h /dev/nvme0n1p2|grep "/"|awk '{print $1,$4}') #[fg=magenta]#(df -h /dev/nvme0n1p3|grep "/"|awk '{print $1,$4}') #[default]%a %d %b %R"
# Window status colors
setw -g status-style bg=black
setw -g status-style fg=colour248

setw -g window-status-current-style bg=colour235
setw -g window-status-current-style fg=colour248

# Clock
setw -g clock-mode-colour colour250
setw -g clock-mode-style 24

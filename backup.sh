#!/bin/bash

HOME_DIR=/home/szhuang
MNT=$HOME_DIR/Experiments
BCK_PATH=$MNT/backups
CON_PATH=$BCK_PATH/configs

#if [ ! -e "$CON_PATH" ]
#then
#	mkdir $CON_PATH
#fi

if [ -e "$CON_PATH" ]
then
    mv $CON_PATH ${CON_PATH}_BAK
fi

mkdir $CON_PATH

echo "Backup to $BCK_PATH"

### Vim
cp -v $HOME_DIR/.vimrc $CON_PATH/vimrc.linux

mkdir $CON_PATH/vim
cp -vR $HOME_DIR/.vim/* $CON_PATH/vim

### Bash
cp -v $HOME_DIR/.bashrc $CON_PATH/bashrc.linux
### Zsh
cp -v $HOME_DIR/.zshrc $CON_PATH/zshrc.linux


### X window
#cp -v $HOME_DIR/.conkyrc $CON_PATH/conkyrc.linux

cp -v $HOME_DIR/.Xresources $CON_PATH/xresources.linux

cp -v $HOME_DIR/.xscreensaver $CON_PATH/xscreensaver.linux

cp -v $HOME_DIR/.xbindkeysrc $CON_PATH/xbindkeysrc.linux

cp -v $HOME_DIR/.xinitrc $CON_PATH/xinitrc.linux

### Mutt
mkdir $CON_PATH/mutt
cp -vR $HOME_DIR/.mutt/* $CON_PATH/mutt

cp -vR $HOME_DIR/.offlineimaprc $CON_PATH

### Awesome WM
cp -vR $HOME_DIR/.config/awesome $CON_PATH/

### Tmux
cp -v $HOME_DIR/.tmux.conf $CON_PATH/tmux.conf

### Termite

cp -v /etc/acpi/handler.sh $CON_PATH/
cp -v /etc/modprobe.d/* $CON_PATH/
cp -v /etc/fstab $CON_PATH/

mkdir $CON_PATH/systemd
cp -vR /etc/systemd/user $CON_PATH/systemd

rm -fR ${CON_PATH}_BAK
